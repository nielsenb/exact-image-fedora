Name:		exact-image
Version:	0.9.1
Release:	3
Summary:	A fast, modern and generic image processing library
Group:		Applications/Multimedia
License:	GPLv2
URL:		http://www.exactcode.com/site/open_source/exactimage/
Source0:	http://dl.exactcode.de/oss/exact-image/exact-image-0.9.1.tar.bz2
BuildRequires:	gcc-c++
BuildRequires:	agg-devel
BuildRequires:	evas-devel
BuildRequires:	libjpeg-turbo-devel
BuildRequires:	libtiff-devel
BuildRequires:	giflib-devel
BuildRequires:	jasper-devel
BuildRequires:	expat-devel
BuildRequires:	OpenEXR-devel
BuildRequires:	lcms-devel
BuildRequires:	libXrender-devel
BuildRequires:  perl-ExtUtils-Embed
Patch0:		exact-image-gnu++98.patch
Patch1:		exact-image-libpng15.patch
Patch2:		exact-image-libgif.patch
Patch3:		ftbfs_evas_object.patch
Patch4:		CVE-2015-3885.patch

%description
In the library we experiment and explore several new algorithms, e.g. for de-screening, data-dependant triangulation scaling, lossless JPEG transforms and others as we see need. You are very much welcome to contribute thrilling state-of-the-art algorithms.

The included codecs take C++ STL std::istreams and std::ostreams allowing library users to implement their own data sources and destinations, such as in memory locations or network transfers.

It is intended to become a modern, generic (template) based C++ library, as time permits. - Hopefully a viable alternative to ImageMagick.

%prep
%setup -q -c -n exact-image
pushd %{name}-%{version}
%patch0 -p1
%patch1 -p1
%patch2 -p1
%patch3 -p1
%patch4 -p1
popd

%build
cd %{name}-%{version}
./configure --prefix=%{buildroot}%{_prefix}
make %{?_smp_mflags}

%install
rm -rf %{buildroot}

cd %{name}-%{version}

make install

%files
%{_bindir}/bardecode
%{_bindir}/econvert
%{_bindir}/edentify
%{_bindir}/empty-page
%{_bindir}/hocr2pdf
%{_bindir}/optimize2bw
%{_bindir}/e2mtiff
%{_bindir}/edisplay

%changelog
* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> 0.9.1-3
- Force gnu++98 for building on Fedora 24

* Thu May 28 2015 Brandon Nielsen <nielsenb@jetfuse.net> 0.9.1-2
- Fix build on Fedora 22
- Fix CVE-2015-3885: Integer overflow in the ljpeg_start function in dcraw

* Tue Mar 17 2015 Brandon Nielsen <nielsenb@jetfuse.net> 0.9.1-1
- Initial specfile
