====================
 exact-image-fedora
====================

Specfile and patches for building and packaging `ExactImage`_ for Fedora.

.. _ExactImage: http://www.exactcode.com/site/open_source/exactimage/
